/*
Bài 1: tính tiền lương nhân viên
Lương 1 ngày: 100.000
Cho người dùng nhập vào số ngày làm
Công thức tính lương: Lương 1 ngày * số ngày làm

Mô hình 3 khối:
1. Input: soNgayLam, luongMotNgay
2. Process:
- Tạo biến soNgayLam, luongMotNgay, tienLuong
- Tạo và gán giá trị cho luongMotNgay
- Tính & xuất tổng lương
3.Output: tongLuong
*/
var soNgayLam;
var tienLuong;
var luongMotNgay = 100000;
function tinhLuong() {
    soNgayLam = document.getElementById("songaylam").value;
    tienLuong = soNgayLam * luongMotNgay;
    console.log("Tổng tiền lương: ", tienLuong);
}

/*
Bài 2: nhập vào 5 số thực
Tính giá trị trung bình của 5 số này và xuất kết quả

Mô hình 3 khối:
1. Input: 5 số thực
2. Process:
- Tạo biến: soThuc1, soThuc2, soThuc3, soThuc4, soThuc5, trungBinhCong
- Nhập giá trị cho soThuc1 - soThuc5
- trungBinhCong = (soThuc1 + soThuc2 + soThuc3 + soThuc4 + soThuc5) / 5
3. Output: giá trị trung bình
*/

var soThu1;
var soThu2;
var soThu3;
var soThu4;
var soThu5;
var trungBinhCong;

function tinhTrungBinhCong() {
    soThu1 = document.getElementById("sothu1");
    soThu2 = document.getElementById("sothu2");
    soThu3 = document.getElementById("sothu3").value;
    soThu4 = document.getElementById("sothu4").value;
    soThu5 = document.getElementById("sothu5").value;
    trungBinhCong = (soThu1 + soThu2);
    console.log("Trung bình cộng của 5 số thực: ", trungBinhCong);
}